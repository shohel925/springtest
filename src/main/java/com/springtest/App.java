package com.springtest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        NoticesDAO noticesDAO = context.getBean(NoticesDAO.class);

        try {
            Notice notice = new Notice(20, "Batch Name1", "email2@gmail.com", "Some new Text2");
            Notice notice2 = new Notice(19, "Batch Name2", "email2@gmail.com", "Some new Text2");
            Notice notice3 = new Notice(22, "Batch Name3", "email2@gmail.com", "Some new Text2");
            List<Notice> notices = new ArrayList<>();
            notices.add(notice);
            notices.add(notice2);
            notices.add(notice3);

            noticesDAO.createNoticeUsingBatch(notices);

            List<Notice> noticess = noticesDAO.getNotices();
            for (Notice noticesss : noticess) {
                System.out.println(noticesss);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }
}
